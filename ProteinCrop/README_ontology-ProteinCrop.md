Ontology - Protein crop
=======================


Motivation
----------



Overview
--------

This ontology has been created for the project Peamust, One of the objectives of this ontology is to be common for several protein crop species. It will need to be updated to 
follow any addition for differents species. These modification are to be described in this directory on this repository

License
-------


Contribution
------------

The scientific coordinator of the Protein crop ontology is [Christophe Lecomte](https://www6.dijon.inra.fr/umragroecologie/Poles-de-Recherches/determinismes-Genetiques-et-Environnementaux-de-l-Adaptation-des-Plantes/FICHES-PERSO/LECOMTE-Christophe) (Inra, Dijon).
The informatics coordinator is [Nacer Mohellibi](https://urgi.versailles.inra.fr/About-us/Team/Information-System-Data-integration/Nacer-Mohellibi) (Inra, Versailles).

If you are interested and wish to know more, or even to contribute, don't hesitate to contact one or both of them!
