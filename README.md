Copyright (c) 2015, INRA  
All rights reserved.  
Each ontology have its own license and owner(s).

This repostiory stores the phenotyping ontologies developped by INRA and its Partner.

# Repository file

The `ontology-repository.json` file list all the ontologies available with the BreedingAPI/CropOntology JSON format. You can specify metadata on every observation variable ontologies like specified in the [Breeding API specifications](https://github.com/plantbreeding/API/blob/master/Specification/ObservationVariables/README.md). The `ontologyName` and the `ontologyDbId` fields in the repository must perfeclty match the same fields on the variables of that ontology in the JSON file.

## Updating ontologies

When updating an ontology, please update the ontology version in the repository file. [Semantic versioning](http://semver.org/) is recommended.

# Structure

```
├── Beet
├── Brassica
├── Maize
├── Melon
├── Miscanthus
├── ProteinCrop
├── Rice
├── Tree
├── Vitis
├── Walnut
├── Wheat
├── Environment
├── Others
├── ontology-repository.json
├── README.md
└── Tools
    ├── archived-DB_Export.tar.gz
    ├── MANAGE_ONTOLOGY
    └── manage_ontology.properties

```

One folder per species group.
> Note: other species groups are likely to be added in the future (example: Solenaceae).

Each folder contains at least the ontology in the CropOntology TDV5 format (excel) and JSON format. It can also contains the ontology in other formats (CSV, Adonis), protocol files, a readme file, a licence file, archives or working directories...
```
├── Tree
│   ├── Adonis_WoodyPlantOntology.zip
│   ├── archive
│   ├── CO_357-Woody Plant Ontology.json
│   ├── T4F_D21_submitted.pdf
│   ├── WoodyPlantOntology.xls
│   └── work
└── Vitis
    ├── archive
    ├── CO_356-Vitis inra ontology.json
    ├── LICENSE
    ├── Liste_des_descripteus_OIV_pour_les_varietes_et_especes_de_vitis__2e_edition_5langues_04_2008.pdf
    ├── README_ontology-vitis.md
    ├── Vitis-inra-ontology.csv
    └── Vitis-inra-ontology.xls
```

The `Others` folder contains automatically generated ontologies from the GnpIS database. The ontologies in this folder will be moved to a dedicated folder once they are curated in the CropOntology TVD5 format.

The `ontology-repository.json` file list all the ontologies available.

The `Tools` folder contains scripts to manage ontologies, in particular the talend `MANAGE_ONTOLOGY` who allows you to check then transform a CropOntology TDV5 format in JSON and Adonis format. 

## Naming recommendations

For each species group there can be several ontologies. For instance, Wheat will have INRA Phenotyping Ontology and Small Grain Cereal Network Ontology.
In this case, each ontology has a group of file with the same name pattern :
- `SpeciesGroup-<ProjectOrComunity>-ontology.xls`
- `SpeciesGroup-<ProjectOrComunity>-ontology.json`
- `SpeciesGroup-<ProjectOrComunity>-ontology.adonis.xml`
- `SpeciesGroup-<ProjectOrComunity>-ontology.owl`

Example :
- `Wheat-inra-ontology.xls`
- `Wheat-small-grain-network-ontology.xls`
