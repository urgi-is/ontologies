package routines;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class FormatDate {

	/**
	 * 
	 * Created on 15/06/2017
	 * @author C. Michotey
	 * 
	 * @param textDate: date to format
	 * @param inAdonisFormat: the format to use is Adonis date format (yyyy-MM-dd) ?
	 * 
	 */
    public static Date doFormatDate(String textDate, Boolean inAdonisFormat) throws Exception {
        if (StringUtils.isNotBlank(textDate)) {
    		String regexDay = "([1-9]|0[1-9]|[12][0-9]|3[01])";
    		String regexMonth = "(0[1-9]|1[012])";
    		String regexYear = "\\d{4}";
    		String regexSplit = "[-/.]";
    			
    		// dd/mm/yyyy or dd-mm-yyyy or dd.mm.yyyy
    		if (textDate.matches("^" + regexDay + regexSplit + regexMonth + regexSplit + regexYear + "$")) {
    			String[] splitDate = (textDate.split(regexSplit));
    			return toDateFormat(splitDate[0], splitDate[1], splitDate[2], inAdonisFormat);
    		}
    		// yyyy/mm/dd or yyyy-mm-dd or yyyy.mm.dd
    		else if (textDate.matches("^" + regexYear + regexSplit + regexMonth + regexSplit + regexDay + "$")) {
    			String[] splitDate = (textDate.split(regexSplit));
    			return toDateFormat(splitDate[2], splitDate[1], splitDate[0], inAdonisFormat);
    		}
    		// mm/dd/yyyy or mm-dd-yyyy or mm.dd.yyyy
    		else if (textDate.matches("^" + regexMonth + regexSplit + regexDay + regexSplit + regexYear + "$")) {
    			String[] splitDate = (textDate.split(regexSplit));
    			return toDateFormat(splitDate[1], splitDate[0], splitDate[2], inAdonisFormat);
    		}
    		else {
    			return null;
	   			//throw new Exception("Date format unknown (" + textDate + ")");
	   		}
		}
   		else {
   			return null;
    	}
    }
    
    private static Date toDateFormat(String day, String month, String year, Boolean inAdonisFormat) {
    	if (inAdonisFormat) {
    		String date = year + "-" + month + "-" + day;
        	return TalendDate.parseDate("yyyy-MM-dd", date);
    	}
    	else {
    		String date = day + "/" + month + "/" + year;
    		return TalendDate.parseDate("dd/MM/yyyy", date);
    	}
    }

}