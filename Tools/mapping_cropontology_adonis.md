# Mapping entre les formats CropOntology et Adonis

Toutes les variables qui ont un **Variable name** d'au moins 6 caractères ou un **Variable synonyms** suffixé par `[Adonis]` peuvent être transformées du format [CropOntology](http://www.cropontology.org/) au format [Adonis](https://www6.inra.fr/adonis/) via le mapping suivant :

* **Variable** Adonis = **Variable** CropOntology

| ADONIS | STATIC | CROPONTOLOGY |
| ------ | ------ | ------------ |
| nom    | -      | 1er valeur de **Variable synonyms** (= nom long de la variable) |
| nomCourt | -    | Variable name |
| nbRepetitionSaisies| `1` | |
| saisieObligatoire | `true` | |
| unite | - | **Scale name** sauf si **Scale classe** = `Nominal` ou `Ordinal` ou `null`, sinon `null` |
| horodatage | `true` | |
| uniteParcoursType | `Individu` | |
| dateCreation | - | Date |
| dateDerniereModification | - | Date |
| commentaire | | Trait description + Method description + Method reference |
| active | | `true` si **Variable status** = `Recommended` ou `Standard for *`, sinon `false` |
| format | `null` | |
| typeVariable | | Scale class |
| ordre | `null` | |
| valeurDefaut | `null` | |
| motsCles | | Variable ID + Variable name + Variable synonyms + Trait class

* **Echelle** Adonis = **Scale** CropOntology

| ADONIS    | CROPONTOLOGY                                                                                              |
| --------- | --------------------------------------------------------------------------------------------------------- |
| nom       | Scale name                                                                                              |
| valeurMin | **Lower limit** si Lower et Upper limit sont remplis                                                      |
| valeurMax | **Upper limit** si Lower et Upper limit sont remplis                                                      |
| exclusif  | `true` si **Scale class** = `Nominal` ou `Ordinal` ou que **Lower et Upper limit** sont remplis, sinon `null` |

* **Notation** Adonis = `Category *` CropOntology au format `valeur = texte` ou `valeur = texte = image`

| ADONIS | CROPONTOLOGY                                                                    |
| ------ | ------------------------------------------------------------------------------- |
| texte  | texte après le 1er "=" (et avant le 2e "=" s'il existe) des champs `Category *` |
| image  | texte après le 2e "=" s'il existe dans les champs `Category *`                  |
| valeur | code avant le 1er "=" des champs `Category *`                                   |


A noter qu'il faut aussi mapper les classes de trait et d'échelle comme ci-dessous :

* Trait class

| CROPONTOLOGY           | ADONIS           |
| ---------------------- | ---------------- |
| Agronomical            | Agronomie        |
| Biochemical            | Biochimie        |
| Environmental          | Environnement    |
| Fertility              | Fertilité        |
| Morphological          | Morphologie      |
| Other                  | null             |
| Phenological           | Phénologie       |
| Physiological          | Physiologie      |
| Quality                | Qualité          |
| Stress                 | Stress           |
| Stress: Abiotic stress | Stress abiotique |
| Stress: Biotic stress  | Stress biotique  |
| *autre*                | *autre*          |

* Scale class

| CROPONTOLOGY                      | ADONIS         |
| --------------------------------- | -------------- |
| Nominal                           | entier         |
| Ordinal                           | entier         |
| Numerical + `Decimal_places = 0`  | entier         |
| Numerical + `Decimal_places != 0` | reel           |
| Text                              | alphanumerique |
| Date                              | date           |
| Duration                          | date           |
| Time                              | date           |
| *autre*                           | alphanumerique |

