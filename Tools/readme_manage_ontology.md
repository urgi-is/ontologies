# How to use `MANAGE_ONTOLOGY` Talend
`MANAGE_ONTOLOGY` is a [Talend](https://fr.talend.com/) project who allows you to check then transform a CropOntology TDV5 format in JSON and Adonis formats.

## 1. Install Talend
Get [Talend Open Studio for Data Integration](https://fr.talend.com/products/data-integration/data-integration-open-studio/) and install it.
> See the documentation [TD_Talend_Good_Practices](https://sites.inra.fr/site/urgi/IS/DEV/Tools/Talend/TD_Talend_Good_Practices.docx?Web=1) for more informations.

## 2. Use the `MANAGE_ONTOLOGY` Talend
1. Get the last version of the [`ontologies` repository](https://forgemia.inra.fr/urgi-is/ontologies).
```
$ cd git/ontologies/
$ git checkout develop ; git pull
```

2. Correctly fill the dedicated properties file `manage_ontology.properties` (store in `git/ontologies/Tools/`).
```
# Ontology IDs
ontologyName=[name of the ontology to manage]
ontologyID=[code or short name of the ontology]

# Input and Output files
ontologyFile=[path + name of the CropOntology TDv5 file to manage]
ontologyTab=[name of the tab sheet containing the ontology to manage]
encoding=[encoding to used to correctly read the data]
photoDir=[if photos are attached to some categories, path of the directory containing those files]
workDir=[path of the directory to work in]

# Generate Adonis format ? (true by default)
adonis=[put 'true' if you want to generate Adonis Variable Library and 'false' if you do not]

# Split excel file ? (false by default)
splitExcel=[put 'true' if you want to split your excel (generate 1 file per language and 1 tab sheet per trait class) and 'false' if you do not]
ontologyTemplate=[path + name of the template to use to split the ontology]
```

Example file:
```
# Ontology IDs
ontologyName=Woody Plant Ontology
ontologyID=CO_357

# Input and Output files
ontologyFile=/home/git/ontologies/Tree/work/WoodyPlantOntology_v2.xls
ontologyTab=WPO
encoding=Windows-1252
photoDir=/home/git/ontologies/Tree/photos/
workDir=/home/git/ontologies/Tree/work/

# Generate Adonis format ? (true by default)
adonis=true

# Split excel file ? (false by default)
splitExcel=true
ontologyTemplate=/home/git/ontologies/Tree/work/WoodyPlantOntology_template.xls
```

3. Import the `MANAGE_ONTOLOGY` project in Talend (store with the properties file in `git/ontologies/Tools/`).
```
Importer un projet existant > nom du projet = `MANAGE_ONTOLOGY` > Sélectionnez le répertoire racine = `git/ontologies/Tools/MANAGE_ONTOLOGY` > Finish
Sélectionner un projet existant > `MANAGE_ONTOLOGY` > Finish
```
> See the documentation [TD_Talend_Good_Practices](https://sites.inra.fr/site/urgi/IS/DEV/Tools/Talend/TD_Talend_Good_Practices.docx?Web=1) for more informations.

4. Launch the orchestration job `Orchestration_manage_TV5` with your customized properties file `manage_ontology.properties`.

5. Check carefully that all went well.

6. Validate the format of the generated JSON file (using [JSONLint](https://jsonlint.com/) for instance)

7. If you can, validate the generated Adonis archive by importing it in Adonis.
