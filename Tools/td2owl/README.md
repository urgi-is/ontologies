## TDv5 to OWL conversion script

convert a Cropontology tdv5 csv or xls to owl/ttl

### Description

### Usage

1. Install 
```
pipenv install
```

2. Launch the script

```
pipenv run ./td2owl.py -c "../../Wheat/CO_321-Wheat Crop Ontology.csv" "../../Wheat/WIPO-Wheat Inra Phenotype Ontology.csv"
```
OR (not recomended)
```
python3 td2owl.py
```
