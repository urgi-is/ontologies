#!/usr/bin/env python
#
#   Requirement:
#       - python 3.X
#       - see requirements.txt
#   Thanks:
#       -https://github.com/SemanticLab/simple-csv-to-rdf/blob/master/convert.py
###################################################################################

import argparse
import csv
from rdflib import Graph, Literal, Namespace, URIRef
from rdflib.namespace import DCTERMS, RDF, RDFS, SKOS, XSD
from pathlib import Path, PurePosixPath

co = Namespace("http://www.cropontology.org/rdf/")

trait_classes={}
def get_trait_class(output_graph, a_row):

    co_prefix=a_row.get('Trait ID').split(":")[0]

    if a_row.get('Trait class') not in trait_classes:
        trait_class_id=co_prefix+":900000"+str(len(trait_classes))
        trait_class_URI = "http://www.cropontology.org/rdf/"+trait_class_id
        trait_classes[a_row.get('Trait class')]=trait_class_URI
        add_triple(output_graph, URIRef(trait_class_URI), RDF.type, co.TraitClass )
        add_triple(output_graph, URIRef(trait_class_URI), co.hasId, Literal(trait_class_id) )
        add_triple(output_graph, URIRef(trait_class_URI), co.hasName, Literal(a_row.get('Trait class')) )

    return trait_classes[a_row.get('Trait class')]

# dict is of the form: label:id
#def add_trait_class_triples(output_graph, a_dict):
#    for a_key in a_dict:
#        trait_class_URI="http://www.cropontology.org/rdf/" + a_dict[a_key]
#        add_triple(output_graph, trait_class_URI, RDF.type, co.TraitClass )
#        add_triple(output_graph, trait_class_URI, co.hasId, Literal(a_dict[a_key]) )
#        add_triple(output_graph, trait_class_URI, co.hasName, Literal(a_key) )


def add_triple(output_graph, s, p, o):
    if o:
        output_graph.add((s, p, o))

def to_literal_lang(a_row, a_key):
    if a_row.get(a_key):
        Literal(a_row.get(a_key), a_row.get('Language'))


def to_literal(a_row, a_key):
    if a_row.get(a_key):
        Literal(a_row.get(a_key))

def load_csv2graph(csv_file) -> Graph():
    # TODO: check it is opened in read only
    input_file = csv.DictReader(open(csv_file), skipinitialspace=True, delimiter=';')
    output_graph = Graph()

    #Will need deduplication latter, schacl might be better adapted
    # variables = dict()
    # traits = dict()
    # methodes = dict()
    # scales = dict()

    for row in input_file:
        # convert it from an OrderedDict to a regular dict, useless in 3.8
        # row = dict(row)
        variableURI = "http://www.cropontology.org/rdf/" + row['Variable ID']
        traitURI = "http://www.cropontology.org/rdf/" + row['Trait ID']
        methodURI = "http://www.cropontology.org/rdf/" + row['Method ID']
        scaleURI = "http://www.cropontology.org/rdf/" + row['Scale ID']
        #Variable

        add_triple(output_graph,URIRef(variableURI), co.hasId, Literal(row['Variable ID']))
        add_triple(output_graph,URIRef(variableURI), RDF.type, co.Variable)
        add_triple(output_graph, URIRef(variableURI), co.hasName, to_literal_lang(row, 'Variable name'))
        add_triple(output_graph, URIRef(variableURI), co.hasLabel, to_literal_lang(row, 'Variable label'))
        for syn in row['Variable synonyms'].split(','):
            add_triple(output_graph,URIRef(variableURI), co.hasSynonym, Literal( syn))
        add_triple(output_graph, URIRef(variableURI), co.hasContext, to_literal_lang(row, 'Context of use'))
        add_triple(output_graph, URIRef(variableURI), co.hasGrowthStage, to_literal_lang(row, 'Growth stage'))
        add_triple(output_graph, URIRef(variableURI), co.hasVariableStatus, to_literal_lang(row, 'Variable status'))
        for v_xref in row['Variable Xref'].split(','):
            add_triple(output_graph,URIRef(variableURI), co.hasVariableXRef, Literal(v_xref))
        for inst in row['Institution'].split(','):
            add_triple(output_graph,URIRef(variableURI), co.hasInstitution, Literal(inst))
        for sci in row['Scientist'].split(','):
            add_triple(output_graph,URIRef(variableURI), co.hasScientist, Literal(sci))
        add_triple(output_graph,URIRef(variableURI), co.hasDate, to_literal(row, 'Date'))
        add_triple(output_graph,URIRef(variableURI), co.hasLanguage, Literal(row['Language'])) # also used for name et al
        add_triple(output_graph,URIRef(variableURI), co.hasCrop, to_literal(row, 'Crop'))
        #link concepts
        add_triple(output_graph,URIRef(variableURI), co.hasTrait, URIRef(traitURI))
        add_triple(output_graph,URIRef(variableURI), co.hasMethod, URIRef(methodURI))
        add_triple(output_graph,URIRef(variableURI), co.hasScale, URIRef(scaleURI))

        #Trait
        add_triple(output_graph,URIRef(traitURI), co.hasTraitId, to_literal(row, 'Trait ID'))
        add_triple(output_graph,URIRef(traitURI), RDF.type, co.Trait)
        add_triple(output_graph,URIRef(traitURI), co.hasName, to_literal_lang(row, 'Trait name'))
        #add_triple(output_graph,URIRef(traitURI), co.hasTraitClass, to_literal(row, 'Trait class'))
        add_triple(output_graph, URIRef(get_trait_class(output_graph, row)) , co.hasTrait, URIRef(traitURI))
        add_triple(output_graph,URIRef(traitURI), co.hasDescription, to_literal_lang(row, 'Trait description'))
        for tsyn in row['Trait synonyms'].split(','):
            add_triple(output_graph,URIRef(traitURI), co.hasSynonym, Literal(tsyn))
        add_triple(output_graph,URIRef(traitURI), co.hasAbbreviation, Literal(row['Main trait abbreviation']))
        for alt_abrev in row['Alternative trait abbreviations'].split(','):
            add_triple(output_graph,URIRef(traitURI), co.hasAltAbbreviation, Literal(alt_abrev))
        add_triple(output_graph,URIRef(traitURI), co.hasEntity, to_literal(row, 'Entity'))
        add_triple(output_graph,URIRef(traitURI), co.hasAttribute, to_literal(row, 'Attribute'))
        add_triple(output_graph,URIRef(traitURI), co.hasTraitStatus, to_literal(row, 'Trait status'))
        for t_xref in row['Trait Xref'].split(','):
            add_triple(output_graph,URIRef(traitURI), co.hasTraitXref, Literal(t_xref))
        # Method
        add_triple(output_graph,URIRef(methodURI), RDF.type, co.Method)
        add_triple(output_graph,URIRef(methodURI), co.hasMethodId, Literal(row['Method ID']))
        add_triple(output_graph,URIRef(methodURI), co.hasName, to_literal_lang(row, 'Method name'))
        add_triple(output_graph,URIRef(methodURI), co.hasMethodClass, Literal(row['Method class']))
        add_triple(output_graph,URIRef(methodURI), co.hasDescription, to_literal_lang(row, 'Method description'))
        add_triple(output_graph,URIRef(methodURI), co.hasFromula, to_literal(row, 'Formula'))
        add_triple(output_graph,URIRef(methodURI), co.hasMethodReference, to_literal(row, 'Method reference'))
        #Scale
        add_triple(output_graph,URIRef(scaleURI), RDF.type, co.Scale)
        add_triple(output_graph,URIRef(scaleURI), co.hasScaleId, to_literal(row, 'Scale ID'))
        add_triple(output_graph,URIRef(scaleURI), co.hasName, Literal(row['Scale name']))
        add_triple(output_graph,URIRef(scaleURI), co.hasScaleClass, Literal(row['Scale class']))
        if 'Decimal places' in row and row['Decimal places'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasDecimalPlaces, Literal(row['Decimal places']))
        if 'Lower limit' in row and row['Lower limit'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasLowerLimit, Literal(row['Lower limit']))
        if 'Upper limit' in row and row['Upper limit'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasUpperLimit, Literal(row['Upper limit']))
        add_triple(output_graph,URIRef(scaleURI), co.hasScaleXref, Literal(row['Scale Xref']))
        if 'Category 1' in row and row['Category 1'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory1, Literal(row['Category 1']))
        if 'Category 2' in row and row['Category 2'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory2, Literal(row['Category 2']))
        if 'Category 3' in row and row['Category 3'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory2, Literal(row['Category 3']))
        if 'Category 4' in row and row['Category 4'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory4, Literal(row['Category 4']))
        if 'Category 5' in row and row['Category 5'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory5, Literal(row['Category 5']))
        if 'Category 6' in row and row['Category 6'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory6, Literal(row['Category 6']))
        if 'Category 7' in row and row['Category 7'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory7, Literal(row['Category 7']))
        if 'Category 8' in row and row['Category 8'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory8, Literal(row['Category 8']))
        if 'Category 9' in row and row['Category 9'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory9, Literal(row['Category 9']))
        if 'Category 10' in row and row['Category 10'] != '':
            add_triple(output_graph,URIRef(scaleURI), co.hasCategory10, Literal(row['Category 10']))
        if row.get('Cat 1 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat1Code, Literal(row['Cat 1 code']))
        if row.get('Cat 1 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat1Description, Literal(row['Cat 1 description']))
        if row.get('Cat 2 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat2Code, Literal(row['Cat 2 code']))
        if row.get('Cat 2 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat2Description, Literal(row['Cat 2 description']))
        if row.get('Cat 3 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat3Code, Literal(row['Cat 3 code']))
        if row.get('Cat 3 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat3Description, Literal(row['Cat 3 description']))
        if row.get('Cat 4 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat4Code, Literal(row['Cat 4 code']))
        if row.get('Cat 4 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat4Description, Literal(row['Cat 4 description']))
        if row.get('Cat 5 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat5Code, Literal(row['Cat 5 code']))
        if row.get('Cat 5 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat5Description, Literal(row['Cat 5 description']))
        if row.get('Cat 6 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat6Code, Literal(row['Cat 6 code']))
        if row.get('Cat 6 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat6Description, Literal(row['Cat 6 description']))
        if row.get('Cat 7 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat7Code, Literal(row['Cat 7 code']))
        if row.get('Cat 7 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat7Description, Literal(row['Cat 7 description']))
        if row.get('Cat 8 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat8Code, Literal(row['Cat 8 code']))
        if row.get('Cat 8 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat8Description, Literal(row['Cat 8 description']))
        if row.get('Cat 9 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat9Code, Literal(row['Cat 9 code']))
        if row.get('Cat 9 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat9Description, Literal(row['Cat 9 description']))
        if row.get('Cat 10 code'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat10Code, Literal(row['Cat 10 code']))
        if row.get('Cat 10 description'):
            add_triple(output_graph,URIRef(scaleURI), co.hasCat10Description, Literal(row['Cat 10 description']))

    # add trait classes to the graph
    #add_trait_class_triples(output_graph, trait_classes)

    return output_graph

def read_graph_and_write_to_file(file_name):
    output_graph = load_csv2graph(file_name)
    f_name = PurePosixPath(file_name).name
    output_file_name = f_name.replace(".csv", ".nt")
    output_graph.serialize(destination=output_file_name, format='nt', encoding='utf-8')


def _main():
    parser = argparse.ArgumentParser(description="""
            Convert a TDV5+ www.cropontology.org csv or excel file to a owl/rdf file .
            """)
    parser.add_argument("-c", "--csv_file", nargs='+', required=True)

    args = parser.parse_args()
    for f in args.csv_file:
        read_graph_and_write_to_file(f)


if __name__ == "__main__":
    _main()
